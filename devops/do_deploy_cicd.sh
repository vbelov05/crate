#!/bin/bash

echo "Starting do_deploy_cicd.sh";
if [[ -z $1 ]]; then
   echo "Empty params";
   exit 1010;
fi

RELEASE=$1;
PATH_GUI="/opt/BacchusGUI";

if [ ! -d ${RELEASE} ]
then
  echo 'Release folder not found.';
  exit 1111;
fi


I=0;
for DIRECT in $(find ${RELEASE}/* -maxdepth 1 -type d -printf "%f\n"); do
	if [ $I -gt 0 ]
	then
		echo "More than one directory found.";
		exit 1112;
	fi
  ((I++))
done

GZ_FILE_NAME=$(ls ${RELEASE}/${DIRECT}/*.gz);

if [ ! -f ${GZ_FILE_NAME} ]
then
	echo "File '*.gz' not found.";
	exit 1113;
fi

if [ -d ${PATH_GUI}/application.new ]
then
  sudo rm -rf ${PATH_GUI}/application.new;
fi

echo "Creation of 'application.new' dirrectory...."
sudo mkdir ${PATH_GUI}/application.new/;

echo "Unpacking release file to  'application.new' dirrectory..."
sudo tar -xzf ${GZ_FILE_NAME} -C ${PATH_GUI}/application.new/;

echo "Copying '.htaccess' file to 'application.new' dirrectory..."
sudo cp ${PATH_GUI}/application.new/config/httpd_$HOSTNAME.htaccess ${PATH_GUI}/application.new/tmbase/.htaccess;

echo "Changing owner and permissions for 'application.new' dirrectory..."
sudo chown -R apache:domain_users ${PATH_GUI}/application.new/;

sudo find ${PATH_GUI}/application.new/* -type d -exec chmod 0755 {} \;
sudo find ${PATH_GUI}/application.new/* -type f -exec chmod 0644 {} \;

echo "Removing 'application.old' dirrectory..."
if [ -d ${PATH_GUI}/application.old ]
then
  sudo rm -rf ${PATH_GUI}/application.old;
fi

echo "Creating 'application' and 'application.old' dirrectories..."
sudo mv ${PATH_GUI}/application ${PATH_GUI}/application.old;
sudo mv ${PATH_GUI}/application.new ${PATH_GUI}/application;

echo "Copying 'latest_deploy.txt' file to project dirrectory..."
sudo cp ${RELEASE}/${DIRECT}/latest_deploy.txt ${PATH_GUI}/;
sudo chown apache:domain_users ${PATH_GUI}/latest_deploy.txt;

echo "Compleate do_deploy_cicd.sh";

#!/bin/bash

BUILD=$1

PATH_SH=/usr/app/sh

case "$BUILD" in
     css)
          chmod +x ${PATH_SH}/gen-js-common-node.sh &&
          ${PATH_SH}/gen-css-main-node.sh
          ;;
     js)
          chmod +x ${PATH_SH}/gen-js-common-node.sh &&
          ${PATH_SH}//gen-js-common-node.sh
          ;;
     gzip)
          chmod +x ${PATH_SH}/gen-js-common-gzip.sh &&
          ${PATH_SH}/gen-js-common-gzip.sh
          ;;
     *)
          echo -e
          echo 'Usage: docker-compose run -e BUILD=(css|js|gzip) nodejs'
          ;;
esac
echo -e

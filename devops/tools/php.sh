#!/bin/bash

BUILD=$1

PATH_SH=/usr/app/sh

case "$BUILD" in
     messages)
          chmod +x ${PATH_SH}/gen-js-messages-php.sh &&
          ${PATH_SH}/gen-js-messages-php.sh
          ;;
     archive)
          chmod +x ${PATH_SH}/gen-deploy-archive-gzip.sh &&
          ${PATH_SH}/gen-deploy-archive-gzip.sh
          ;;
     *)
          echo -e
          echo 'Usage: docker-compose run -e BUILD=(messages|archive) php'
          ;;
esac
echo -e

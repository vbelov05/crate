#!/bin/bash

PROJECT_PATH=${WORKING_DIR}'/gui'

TMBASE='tmbase'

PRE_COMMON_JS=${PROJECT_PATH}'/'${TMBASE}'/_gen/pre_common.js'
COMMON_JS=${PROJECT_PATH}'/'${TMBASE}'/_gen/common.js'
DHTMLX_JS=${PROJECT_PATH}'/'${TMBASE}'/_gen/dhtmlx.js'

OUTPUT_FOLDER=${WORKING_DIR}'/archive'

